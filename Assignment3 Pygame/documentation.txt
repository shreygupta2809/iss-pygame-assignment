THE ENTIRE GAME WAS DEVELOPED IN WINDOWS

--General Keys-- 
Press 'Space' to pause/unpause the game. 

--Gameplay-- 
There are 2 players in the game. 
1st Player starts from the bottom and has to go to the top to finish the level. 
2nd Player starts from the top and has to go to the bottom to finish the level. 
Turn will go to the other player if a player collides with an obstacle or if the player runs out of time or if a player is able to complete his/her level. 
 
--Controls-- 
Both the players can move in any direction with the help of the arrow keys. 
 
--Obstacles-- 
There are 2 types of obstacles: 
1. Ships: Moving 
	The ships change direction as soon as they reach either end of the map. Also, the speed of the ships increases as the level of the player increases.
2. Zombies: Static 
 
--Scoring-- 
There is a round score which shows the score that the current player accumulates in that round.
Round score will increase by 5 points on crossing a zombie layer. 
Round score will increase by 10 points on crossing a ship layer.
There is also a countdown of 60 seconds in the center which shows time left for that round.
If a player is able to complete that level ,the round score is added to his total score along with a getting a time bonus which is the amount of time left from 60 seconds.
In case the player collides with an obstacle, he/she will get the round score accumulated until that moment but will not get any time bonus. 
There is also a penalty of 20 points in case the player collides with an obstacle.
If he/she runs out of time the turn is switched and that player gets no points for that round.
 
--Winner(Declared when the game is quit)-- 
The player which has the higher level is declared the winner at the end of the game.
In case both the players are on the same level, the winner is decided on the basis of the score(higher is the winner).
If both the level and score are same then it is declared as 'TIE GAME'.

Developed by:
Shrey Gupta
2019101058
