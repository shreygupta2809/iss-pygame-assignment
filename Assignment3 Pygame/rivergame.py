import pygame
import random
import configparser

pygame.mixer.pre_init(44100, -16, 2, 1024)
pygame.mixer.init()
pygame.init()


f = configparser.RawConfigParser()
f.read('config.ini')

ftype = f.get('font', 'type')
fsize1 = int(f.get('font', 'size1'))
fsize2 = int(f.get('font', 'size2'))
c1 = eval(f.get('color', 'black'))
c2 = eval(f.get('color', 'yellow'))
c3 = eval(f.get('color', 'red'))
c4 = eval(f.get('color', 'white'))
c5 = eval(f.get('color', 'green'))
c6 = eval(f.get('color', 'back'))
m1 = f.get('message', 'win1')
m2 = f.get('message', 'win2')
m3 = f.get('message', 'p1')
m4 = f.get('message', 'p2')
m5 = f.get('message', 'tim')
m6 = f.get('message', 'tie')
m7 = f.get('message', 'sco')
m8 = f.get('message', 'clear')
m9 = f.get('message', 'crash')
m10 = f.get('message', 'pause')
m11 = f.get('message', 'lev')

win = pygame.display.set_mode((500, 500))
pygame.display.set_caption("Crossing")
clock = pygame.time.Clock()
maxnum = 5
statobstacles = pygame.sprite.Group()
movobstacles = pygame.sprite.Group()
currplayer = 0
currscore = 0
timeleft = 60
run = True
count = 0
crossed = 0
font = pygame.font.Font(ftype, fsize1)
font1 = pygame.font.Font(ftype, fsize2)
pygame.mixer.music.load('back.mp3')
pygame.mixer.music.play(-1)
sound = pygame.mixer.Sound('zomb.wav')


class StatObj(pygame.sprite.Sprite):
    def __init__(self, x, y, width, height):
        super().__init__()
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.image = pygame.image.load("zomb.png")

    def drawstat(self, win):
        win.blit(self.image, (self.x, self.y))


class MovObj(StatObj):
    def __init__(self, x, y, width, height, direc):
        StatObj.__init__(self, x, y, width, height)
        self.vel = 2
        self.direc = direc
        self.image = pygame.image.load("ship2.png")

    def drawmov(self, win):
        win.blit(self.image, (self.x, self.y))


class Player(StatObj):
    def __init__(self, x, y, width, height):
        StatObj.__init__(self, x, y, width, height)
        self.vel = 3
        self.score = 0
        self.level = 0
        self.image = pygame.image.load("player.png")

    def draw1(self, win):
        win.blit(self.image, (self.x, self.y))


players = [Player(250, 460, 24, 24), Player(250, 0, 24, 24)]


def background():
    for i in range(0, 6):
        pygame.draw.rect(win, c5, (0, 92*i, 500, 40))


def drawstatobstacle():
    for ob in statobstacles:
        ob.drawstat(win)


def drawmovobstacles():
    for mob in movobstacles:
        if mob.direc == 0:
            if mob.x >= mob.vel + players[currplayer].level / 2:
                mob.x -= (mob.vel + players[currplayer].level / 2)
            else:
                mob.direc = 1

        if mob.direc == 1:
            temp = players[currplayer].level / 2
            if mob.x + mob.vel + temp + mob.width <= 480:
                mob.x += (mob.vel + players[currplayer].level / 2)
            else:
                mob.direc = 0
        mob.drawmov(win)


def obstacles_generate():
    statobstacles.__init__()
    movobstacles.__init__()
    for i in range(1, 5):
        num = random.randint(1, maxnum)
        for j in range(0, num):
            xcord = random.randrange(0, 460, 32)
            statobstacles.add(StatObj(xcord, 92*i, 40, 40))
    for i in range(0, 5):
        direc = random.randint(0, 1)
        xcord = random.randrange(0, 470, 32)
        movobstacles.add(MovObj(xcord, 40 + 92*i, 30, 30, direc))


obstacles_generate()


def col1():
    for mob in movobstacles:
        if abs(mob.y - players[currplayer].y) < 35:
            if abs(mob.x - players[currplayer].x) < 35:
                sound.play()
                return True
    return False


def col2():
    for ob in statobstacles:
        if abs(ob.y - players[currplayer].y) < 35:
            if abs(ob.x - players[currplayer].x) < 30:
                sound.play()
                return True
    return False


def change():
    global timeleft
    global currplayer
    global currscore
    global crossed
    crossed = 0
    currplayer = 1 - currplayer
    players[currplayer].x = 250
    if currplayer == 0:
        players[currplayer].y = 460
    if currplayer == 1:
        players[currplayer].y = 0
    obstacles_generate()
    currscore = 0
    timeleft = 60


def score():
    text1 = font.render(m3 + str(players[0].score), True, c3)
    text2 = font.render(m4 + str(players[1].score), True, c3)
    text3 = font.render(m5 + str(timeleft), True, c3)
    text4 = font.render(m7 + str(currscore), True, c3)
    text5 = font.render(m11 + str(players[currplayer].level), True, c3)
    textrect1 = text1.get_rect()
    textrect2 = text2.get_rect()
    textrect2.x = 0
    textrect2.y = 10
    textrect1.x = 0
    textrect1.y = 470
    textrect3 = text3.get_rect()
    textrect4 = text4.get_rect()
    textrect5 = text5.get_rect()
    textrect3.x = 200
    textrect3.y = 10
    textrect4.x = 370
    textrect4.y = 10
    textrect5.x = 370
    textrect5.y = 470
    win.blit(text1, textrect1)
    win.blit(text2, textrect2)
    win.blit(text3, textrect3)
    win.blit(text4, textrect4)
    win.blit(text5, textrect5)


def display():
    win.fill(c6)
    background()
    score()
    drawstatobstacle()
    drawmovobstacles()
    players[currplayer].draw1(win)
    pygame.display.update()


while run:
    clock.tick(60)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
            if players[0].level == players[1].level:
                if players[0].score == players[1].score:
                    win.fill(c1)
                    text = font1.render(m6, True, c2)
                    textrect = text.get_rect()
                    textrect.x = 180
                    textrect.y = 245
                    win.blit(text, textrect)
                    pygame.display.update()
                    pygame.time.delay(1000)
                    pygame.quit()
                if players[0].score > players[1].score:
                    win.fill(c1)
                    text = font1.render(m1, True, c2)
                    textrect = text.get_rect()
                    textrect.x = 120
                    textrect.y = 240
                    win.blit(text, textrect)
                    pygame.display.update()
                    pygame.time.delay(1000)
                    pygame.quit()
                if players[0].score < players[1].score:
                    win.fill(c1)
                    text = font1.render(m2, True, c2)
                    textrect = text.get_rect()
                    textrect.x = 120
                    textrect.y = 240
                    win.blit(text, textrect)
                    pygame.display.update()
                    pygame.time.delay(1000)
                    pygame.quit()
            elif players[0].level > players[1].level:
                win.fill(c1)
                text = font1.render(m1, True, c2)
                textrect = text.get_rect()
                textrect.x = 120
                textrect.y = 240
                win.blit(text, textrect)
                pygame.display.update()
                pygame.time.delay(1000)
                pygame.quit()
            elif players[0].level < players[1].level:
                win.fill(c1)
                text = font1.render(m2, True, c2)
                textrect = text.get_rect()
                textrect.x = 120
                textrect.y = 240
                win.blit(text, textrect)
                pygame.display.update()
                pygame.time.delay(1000)
                pygame.quit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                pause = True
                while pause:
                    win.fill(c1)
                    text = font.render(m10, True, c4)
                    textrect = text.get_rect()
                    textrect.x = 170
                    textrect.y = 250
                    win.blit(text, textrect)
                    pygame.display.update()
                    for event in pygame.event.get():
                        if event.type == pygame.KEYDOWN:
                            if event.key == pygame.K_SPACE:
                                pause = False
                        if event.type == pygame.QUIT:
                            pygame.quit()
    if count == 60:
        timeleft -= 1
        count = 0

    if timeleft == 0:
        change()
    count += 1
    if currplayer == 1:
        if players[1].y > 92 and crossed == 0:
            crossed += 1
            currscore += 10
        elif players[1].y > 132 and crossed == 1:
            crossed += 1
            currscore += 5
        elif players[1].y > 184 and crossed == 2:
            crossed += 1
            currscore += 10
        elif players[1].y > 224 and crossed == 3:
            crossed += 1
            currscore += 5
        elif players[1].y > 274 and crossed == 4:
            crossed += 1
            currscore += 10
        elif players[1].y > 314 and crossed == 5:
            crossed += 1
            currscore += 5
        elif players[1].y > 366 and crossed == 6:
            crossed += 1
            currscore += 10
        elif players[1].y > 406 and crossed == 7:
            crossed += 1
            currscore += 5
        elif players[1].y > 450 and crossed == 8:
            crossed += 1
            currscore += 10
    if currplayer == 0:
        if players[0].y < 15 and crossed == 8:
            crossed += 1
            currscore += 10
        elif players[0].y < 63 and crossed == 7:
            crossed += 1
            currscore += 5
        elif players[0].y < 98 and crossed == 6:
            crossed += 1
            currscore += 10
        elif players[0].y < 155 and crossed == 5:
            crossed += 1
            currscore += 5
        elif players[0].y < 194 and crossed == 4:
            crossed += 1
            currscore += 10
        elif players[0].y < 241 and crossed == 3:
            crossed += 1
            currscore += 5
        elif players[0].y < 285 and crossed == 2:
            crossed += 1
            currscore += 10
        elif players[0].y < 338 and crossed == 1:
            crossed += 1
            currscore += 5
        elif players[0].y < 380 and crossed == 0:
            crossed += 1
            currscore += 10

    keys = pygame.key.get_pressed()
    if keys[pygame.K_LEFT]:
        players[currplayer].x -= players[currplayer].vel
        if players[currplayer].x <= 10:
            players[currplayer].x = 10

    if keys[pygame.K_RIGHT]:
        players[currplayer].x += players[currplayer].vel
        if players[currplayer].x + players[currplayer].width >= 490:
            players[currplayer].x = 490 - players[currplayer].width

    if keys[pygame.K_UP]:
        players[currplayer].y -= players[currplayer].vel
        if players[currplayer].y <= 0 and currplayer == 0:
            players[currplayer].y = 0
        elif players[currplayer].y <= 00 and currplayer == 1:
            players[currplayer].y = 00

    if keys[pygame.K_DOWN]:
        players[currplayer].y += players[currplayer].vel
        if players[currplayer].y >= 460 and currplayer == 0:
            players[currplayer].y = 460
        elif players[1].y + players[1].height >= 490:
            players[currplayer].y = 490 - players[currplayer].height

    if col1() or col2():
        players[currplayer].score += currscore
        players[currplayer].score -= 20
        if players[currplayer].score <= 0:
            players[currplayer].score = 0
        win.fill((0, 0, 0))
        text = font.render(m9, True, c4)
        textrect = text.get_rect()
        textrect.x = 170
        textrect.y = 250
        win.blit(text, textrect)
        pygame.display.update()
        pygame.time.wait(1000)
        change()

    if currplayer == 0 and players[currplayer].y == 10:
        players[currplayer].score += timeleft
        players[currplayer].score += currscore
        players[currplayer].level += 1
        win.fill((0, 0, 0))
        text = font.render(m8, True, c4)
        textrect = text.get_rect()
        textrect.x = 170
        textrect.y = 250
        win.blit(text, textrect)
        pygame.display.update()
        pygame.time.wait(1000)
        change()

    if currplayer == 1:
        if players[currplayer].y + players[currplayer].height >= 485:
            players[currplayer].score += timeleft
            players[currplayer].score += currscore
            players[currplayer].level += 1
            win.fill((0, 0, 0))
            text = font.render(m8, True, c4)
            textrect = text.get_rect()
            textrect.x = 170
            textrect.y = 250
            win.blit(text, textrect)
            pygame.display.update()
            pygame.time.wait(1000)
            change()

    display()
pygame.quit()
